﻿using System;
using System.Threading.Tasks;
using BlazorE2E.Tests.Infrastructure;
using Microsoft.Playwright;
using Xunit;

namespace BlazorE2E.Tests.Tests.Client.Pages
{
	public class Counter_Should : IClassFixture<WebApplicationFactoryFixture>
	{
        public Counter_Should(WebApplicationFactoryFixture factoryFixture)
		{
            factoryFixture.CreateDefaultClient();
        }

		[Fact]
		public async Task Increase_On_Click()
        {
            using var playwright = await Playwright.CreateAsync();
            await using var browser = await playwright.Chromium.LaunchAsync(new BrowserTypeLaunchOptions
            {
                Headless = false,
                SlowMo = 1000
            });
            var context = await browser.NewContextAsync();
            // Open new page
            var page = await context.NewPageAsync();
            // Go to https://localhost:7048/
            await page.GotoAsync("https://localhost:7048/");
            // Click text=Counter
            await page.ClickAsync("text=Counter");
            // Assert.AreEqual("https://localhost:7048/counter", page.Url);
            // Click text=Current count: 0
            await page.ClickAsync("text=Current count: 0");
            // Click text=Click me
            await page.ClickAsync("text=Click me");
            // Click text=Current count: 1
            await page.ClickAsync("text=Current count: 1");
        }
	}
}

