﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;

namespace BlazorE2E.Tests.Infrastructure
{
    public class WebApplicationFactoryFixture : WebApplicationFactory<Program>
    {
        public WebApplicationFactoryFixture()
        {
        }

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.UseUrls("https://localhost:7048");
        }

        protected override IHost CreateHost(IHostBuilder builder)
        {
            // need to create a plain host that we can return.
            var dummyHost = builder.Build();

            // configure and start the actual host.
            builder.ConfigureWebHost(webHostBuilder => webHostBuilder.UseKestrel());

            var host = builder.Build();
            host.Start();
            
            return dummyHost;
        }
    }
}

